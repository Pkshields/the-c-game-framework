////////////////////////////////////////////////////////////////////////////////
// Filename: Vector3f.cpp
////////////////////////////////////////////////////////////////////////////////

#include "Vector3f.h"

using namespace CGF;

/**
 * Constructor for the Vector3f
 */
Vector3f::Vector3f()
{
	this->X = 0.0f;
	this->Y = 0.0f;
	this->Z = 0.0f;
}

/**
 * Copy Constructor for the Vector3f
 *
 * copy		Vector to copy
 */
Vector3f::Vector3f(const Vector3f &copy)
{
	X = copy.X;
	Y = copy.Y;
	Z = copy.Z;
}

/**
 * Constructor for the Vector3f
 *
 * @x	Position in the X axis
 * @y	Position in the Y axis
 * @z	Position in the Z axis
 */
Vector3f::Vector3f(float x, float y, float z)
{
	this->X = x;
	this->Y = y;
	this->Z = z;
}

/**
 * Destructor for the Vector3f
 */
Vector3f::~Vector3f()
{
}

/**
 * Calculate the length for the vectior
 */
float Vector3f::Length()
{
	return sqrt((X * X) + (Y * Y) + (Z * Z));
}

/**
 * Normalize this vector
 */
void Vector3f::Normalize()
{
	float length = Length();

	if (length != 0)
	{
		X /= length;
		Y /= length;
		Z /= length;
	}
}

/**
 * = operator for the Vector3f
 *
 * @param	Vector3f to copy
 */
Vector3f& Vector3f::operator=(const Vector3f &param)
{
	if (this == &param)
      return *this;

	X = param.X;
	Y = param.Y;
	Z = param.Z;

	return *this;
}

/**
 * += operator for the Vector3f
 *
 * @param	Vector3f to add
 */
Vector3f& Vector3f::operator+= (const Vector3f &param)
{
	X += param.X;
	Y += param.Y;
	Z += param.Z;

	return *this;
}

/**
 * -= operator for the Vector3f
 *
 * @param	Vector3f to subtract
 */
Vector3f& Vector3f::operator-= (const Vector3f &param)
{
	X -= param.X;
	Y -= param.Y;
	Z -= param.Z;

	return *this;
}

/**
 * *= operator for the Vector3f
 *
 * @param	Scalar to multiply
 */
Vector3f& Vector3f::operator*= (const float &param)
{
	X *= param;
	Y *= param;
	Z *= param;

	return *this;
}

/**
 * /= operator for the Vector3f
 *
 * @param	Scalar to divide
 */
Vector3f& Vector3f::operator/= (const float &param)
{
	X /= param;
	Y /= param;
	Z /= param;

	return *this;
}

/**
 * + operator for the Vector3f
 *
 * @param	Vector3f to add
 */
const Vector3f Vector3f::operator+ (const Vector3f &param) const
{
	Vector3f result = *this; 
	result += param;
	return result;
}

/**
 * - operator for the Vector3f
 *
 * @param	Vector3f to subtract
 */
const Vector3f Vector3f::operator- (const Vector3f &param) const
{
	Vector3f result = *this;
	result -= param;
	return result;
}

/**
 * * operator for the Vector3f
 *
 * @param	Scalar to multiply
 */
const Vector3f Vector3f::operator* (const float &param) const
{
	Vector3f result = *this;
	result *= param;
	return result;
}

/**
 * / operator for the Vector3f
 *
 * @param	Scalar to divide
 */
const Vector3f Vector3f::operator/ (const float &param) const
{
	Vector3f result = *this;
	result /= param;
	return result;
}

/**
 * * operator for the Vector3f, in the other direction
 *
 * @param	Scalar to multiply
 */
inline Vector3f operator* (float s, const Vector3f& v)
{
	return Vector3f(s * v.X, s * v.Y, s * v.Z);
}

/**
 * / operator for the Vector3f, in the other direction
 *
 * @param	Scalar to divide
 */
inline Vector3f operator/ (float s, const Vector3f& v)
{
	return Vector3f(v.X / s, v.Y / s, s / v.Z);
}