////////////////////////////////////////////////////////////////////////////////
// Filename: Window.cpp
////////////////////////////////////////////////////////////////////////////////

#include "Window.h"

using namespace CGF;

/**
 * Create an instance of the Window
 */
Window::Window()
{
}

/**
 * Deconstructor of the Window
 */
Window::~Window()
{
}

/**
 * Initialize the WindowManager
 *
 * @param width			Width of the window.
 * @param height		Height of the window
 * @param isFullscreen	Is the window fullscreen? (ignores the above values if it is)
 */
bool Window::Initialize(const int width, const int height, const bool isFullscreen)
{
	//Check if we have already created a window or not
	if (m_currentWindow == 0)
	{
		//Objects. Holds some settings for the screen
		WNDCLASSEX wc;
		DEVMODE dmScreenSettings;
		int posX, posY;

		//GetModuleHandle = Retrieves a module handle for the specified module. The module must have been loaded by the calling process.
		//I think it is a pointer to the application EXE?
		//Module is a EXE or DLL. Gets a referecne to this one for some reason
		m_hInstance = GetModuleHandle(NULL);

		//Give the application a name.
		//LPCWSTR = Long Pointer to Constant Wide String = 32-bit pointer to a constant string of 16-bit Unicode characters
		m_applicationName = L"Engine";

		//Setup the windows class with default settings.
		wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;				//The class styles/Window Class Styles - define additional elements of the window class.
		wc.lpfnWndProc = WindowProcecure;							//An application-defined function that processes messages sent to a window.
		wc.cbClsExtra = 0;											//Extra bytes to allocate to CLASS data structure in USER.EXE local heap when RegisterClass() is called.
		wc.cbWndExtra = 0;											//Extra bytes to allocate to WND data structure in USER.EXE local heap when CreateWindow() is called.
		wc.hInstance = m_hInstance;									//Unique identifier for this DLL file?
		wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);						//handle to a single icon image (for the window)
		wc.hIconSm = wc.hIcon;										//Small version of the image
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);					//Cursor to use when the cursor is inside the window
		wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);		//Default background colour
		wc.lpszMenuName = NULL;										//
		wc.lpszClassName = m_applicationName;						//Title name for the window
		wc.cbSize = sizeof(WNDCLASSEX);								//Size of the WNDCLASSEX structure itself. Used as a precautiuon for pointers

		//Register the window class.
		RegisterClassEx(&wc);

		//Determine the resolution of the clients desktop screen.
		int screenWidth  = GetSystemMetrics(SM_CXSCREEN);
		int screenHeight = GetSystemMetrics(SM_CYSCREEN);

		//Setup the screen settings depending on whether it is running in full screen or in windowed mode.
		if(isFullscreen)
		{
			//If full screen set the screen to maximum size of the users desktop and 32bit.
			memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
			dmScreenSettings.dmSize       = sizeof(dmScreenSettings);
			dmScreenSettings.dmPelsWidth  = (unsigned long)screenWidth;
			dmScreenSettings.dmPelsHeight = (unsigned long)screenHeight;
			dmScreenSettings.dmBitsPerPel = 32;			
			dmScreenSettings.dmFields     = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

			//Change the display settings to full screen.
			ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);

			//Set the position of the window to the top left corner.
			posX = posY = 0;
		}
		else
		{
			//If windowed then set it to passed in resolution.
			screenWidth  = width;
			screenHeight = height;

			//Place the window in the middle of the screen.
			posX = (GetSystemMetrics(SM_CXSCREEN) - screenWidth)  / 2;
			posY = (GetSystemMetrics(SM_CYSCREEN) - screenHeight) / 2;
		}

		//Create the window with the screen settings and get the handle to it.
		if (!isFullscreen)
			m_hwnd = CreateWindowEx(WS_EX_APPWINDOW, m_applicationName, m_applicationName, 
						WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
						posX, posY, screenWidth, screenHeight, NULL, NULL, m_hInstance, NULL);
		else
			m_hwnd = CreateWindowEx(WS_EX_APPWINDOW, m_applicationName, m_applicationName, 
						WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP,
						posX, posY, screenWidth, screenHeight, NULL, NULL, m_hInstance, NULL);

		//Bring the window up on the screen and set it as main focus.
		ShowWindow(m_hwnd, SW_SHOW);
		SetForegroundWindow(m_hwnd);
		SetFocus(m_hwnd);

		//Store if this is fullscreen or not
		this->m_isFullscreen = isFullscreen;
		m_currentWindow = this;

		//Hide the mouse cursor.
		//ShowCursor(false);

		return true;
	}
	else
		//Window already created, return false
		return false;
}

/*
 * Shut down the WindowManager
 */
bool Window::Shutdown()
{
	//Show the mouse cursor.
	ShowCursor(true);

	//Fix the display settings if leaving full screen mode.
	if(m_isFullscreen)
		ChangeDisplaySettings(NULL, 0);

	//Remove the window.
	DestroyWindow(m_hwnd);
	m_hwnd = NULL;

	//Remove the application instance.
	UnregisterClass(m_applicationName, m_hInstance);
	m_hInstance = NULL;

	//Release the pointer to this class.
	m_currentWindow = NULL;

	return true;
}

/**
 * This function handles the many messages that an application receives from the operating system
 */
LRESULT CALLBACK Window::WindowProcecure(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam)
{
	switch(umessage)
	{
		//Check if the window is being destroyed.
		case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}

		//Check if the window is being closed.
		case WM_CLOSE:
		{
			PostQuitMessage(0);		
			return 0;
		}

		//All other messages pass to the message handler in the system class.
		default:
			return m_currentWindow->MessageHandler(hwnd, umessage, wparam, lparam);
	}
}

/**
 * Handle the data sent back from WndProcecure
 */
LRESULT CALLBACK Window::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
{
	//Let the default message handler use the message as we aren't using them (directly)
	return DefWindowProc(hwnd, umsg, wparam, lparam);
}