////////////////////////////////////////////////////////////////////////////////
// Filename: Window.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include <windows.h>
#include "CGFInclude.h"

namespace CGF
{
	class CGF_API Window
	{
	public:
		Window();
		~Window();

		bool Initialize(const int width, const int height, const bool isFullscreen);
		bool Shutdown();

		static LRESULT CALLBACK WindowProcecure(HWND, UINT, WPARAM, LPARAM);
		LRESULT CALLBACK MessageHandler(HWND, UINT, WPARAM, LPARAM);

		HWND* GetWindowHandlePtr() { return &m_hwnd; }
		HWND GetWindowHandle() { return m_hwnd; }
		void SetMouseVisible(const bool isVisible) { ShowCursor(isVisible); }
		HINSTANCE* GetHInstancePtr() { return &m_hInstance; }
		HINSTANCE GetHInstance() { return m_hInstance; }

	private:
		HINSTANCE m_hInstance;					/**< Unique identifier for this DLL file */
		LPCWSTR m_applicationName;				/**< Title for the window */
		HWND m_hwnd;							/**< Handle for the window itself */

		bool m_isFullscreen;					/**< Is the window fullscreen? */
	};

	static Window* m_currentWindow = 0;			/**< Current Window, used in the WndProcecure method */
}