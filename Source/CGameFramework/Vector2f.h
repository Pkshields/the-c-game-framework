////////////////////////////////////////////////////////////////////////////////
// Filename: Vector2f.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include <math.h>
#include "CGFInclude.h"
#include <DirectXMath.h>

using namespace DirectX;

namespace CGF
{
	class CGF_API Vector2f
	{
	public:
		Vector2f();
		Vector2f(const Vector2f &copy);
		Vector2f(float x, float y);
		~Vector2f();

		float Length();
		void Normalize();

		Vector2f& operator=(const Vector2f &param);
		Vector2f& operator+= (const Vector2f &param);
		Vector2f& operator-= (const Vector2f &param);
		Vector2f& operator*= (const float &param);
		Vector2f& operator/= (const float &param);
		const Vector2f operator+ (const Vector2f &param) const ;
		const Vector2f operator- (const Vector2f &param)const ;
		const Vector2f operator* (const float &param)const ;
		const Vector2f operator/ (const float &param)const ;

	public:
		float X;
		float Y;
	};
}