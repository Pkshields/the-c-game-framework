////////////////////////////////////////////////////////////////////////////////
// Filename: Vector3f.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include <math.h>
#include "CGFInclude.h"
#include <DirectXMath.h>

using namespace DirectX;

namespace CGF
{
	class CGF_API Vector3f
	{
	public:
		Vector3f();
		Vector3f(const Vector3f &copy);
		Vector3f(float x, float y, float z);
		~Vector3f();

		float Length();
		void Normalize();

		Vector3f& operator=(const Vector3f &param);
		Vector3f& operator+= (const Vector3f &param);
		Vector3f& operator-= (const Vector3f &param);
		Vector3f& operator*= (const float &param);
		Vector3f& operator/= (const float &param);
		const Vector3f operator+ (const Vector3f &param) const ;
		const Vector3f operator- (const Vector3f &param)const ;
		const Vector3f operator* (const float &param)const ;
		const Vector3f operator/ (const float &param)const ;

	public:
		float X;
		float Y;
		float Z;
	};
}