////////////////////////////////////////////////////////////////////////////////
// Filename: m_matrix.cpp
////////////////////////////////////////////////////////////////////////////////

#include "matrix.h"

using namespace CGF;

/**
 * Constructor for the Matrix
 */
Matrix::Matrix()
{
	XMStoreFloat4x4(&m_matrix, XMMatrixIdentity());
}

/**
 * Copy Constructor for the Matrix
 *
 * copy		Vector to copy
 */
Matrix::Matrix(const Matrix &copy)
{
	m_matrix = copy.m_matrix;
}

/**
 * Constructor for the Matrix
 *
 * @m11		Value for the matrix in position m11
 * @m12		Value for the matrix in position m12
 * @m13		Value for the matrix in position m13
 * @m14		Value for the matrix in position m14
 * @m21		Value for the matrix in position m21
 * @m22		Value for the matrix in position m22
 * @m23		Value for the matrix in position m23
 * @m24		Value for the matrix in position m24
 * @m31		Value for the matrix in position m31
 * @m32		Value for the matrix in position m32
 * @m33		Value for the matrix in position m33
 * @m34		Value for the matrix in position m34
 * @m41		Value for the matrix in position m41
 * @m42		Value for the matrix in position m42
 * @m43		Value for the matrix in position m43
 * @m44		Value for the matrix in position m44
 */
Matrix::Matrix(float m11, float m12, float m13, float m14,
			   float m21, float m22, float m23, float m24, 
			   float m31, float m32, float m33, float m34, 
			   float m41, float m42, float m43, float m44)
{
	m_matrix._11 = m11;
	m_matrix._12 = m12;
	m_matrix._13 = m13;
	m_matrix._14 = m14;

	m_matrix._21 = m21;
	m_matrix._22 = m22;
	m_matrix._23 = m23;
	m_matrix._24 = m24;

	m_matrix._31 = m31;
	m_matrix._32 = m32;
	m_matrix._33 = m33;
	m_matrix._34 = m34;

	m_matrix._41 = m41;
	m_matrix._42 = m42;
	m_matrix._43 = m43;
	m_matrix._44 = m44;
}

/**
 * Copy Constructor for the Matrix
 *
 * copy		XM struct to copy
 */
Matrix::Matrix(XMFLOAT4X4 &copy)
{
	m_matrix = copy;
}

/**
 * Destructor for the Matrix
 */
Matrix::~Matrix()
{
}

/**
 * Get the identity matrix
 */
Matrix Matrix::Identity()		//STATIC
{
	XMFLOAT4X4 matrix;
	XMStoreFloat4x4(&matrix, XMMatrixIdentity());
	return Matrix(matrix);
}

/**
 * Get the transpose of this matrix
 */
Matrix Matrix::Inverse()
{
	XMFLOAT4X4 result;

	float s0 = m_matrix.m[0][0] * m_matrix.m[1][1] - m_matrix.m[1][0] * m_matrix.m[0][1];
	float s1 = m_matrix.m[0][0] * m_matrix.m[1][2] - m_matrix.m[1][0] * m_matrix.m[0][2];
	float s2 = m_matrix.m[0][0] * m_matrix.m[1][3] - m_matrix.m[1][0] * m_matrix.m[0][3];
	float s3 = m_matrix.m[0][1] * m_matrix.m[1][2] - m_matrix.m[1][1] * m_matrix.m[0][2];
	float s4 = m_matrix.m[0][1] * m_matrix.m[1][3] - m_matrix.m[1][1] * m_matrix.m[0][3];
	float s5 = m_matrix.m[0][2] * m_matrix.m[1][3] - m_matrix.m[1][2] * m_matrix.m[0][3];

	float c5 = m_matrix.m[2][2] * m_matrix.m[3][3] - m_matrix.m[3][2] * m_matrix.m[2][3];
	float c4 = m_matrix.m[2][1] * m_matrix.m[3][3] - m_matrix.m[3][1] * m_matrix.m[2][3];
	float c3 = m_matrix.m[2][1] * m_matrix.m[3][2] - m_matrix.m[3][1] * m_matrix.m[2][2];
	float c2 = m_matrix.m[2][0] * m_matrix.m[3][3] - m_matrix.m[3][0] * m_matrix.m[2][3];
	float c1 = m_matrix.m[2][0] * m_matrix.m[3][2] - m_matrix.m[3][0] * m_matrix.m[2][2];
	float c0 = m_matrix.m[2][0] * m_matrix.m[3][1] - m_matrix.m[3][0] * m_matrix.m[2][1];

	float det = s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0;

	if (det == 0.f)
		return Matrix(0.f, 0.f, 0.f, 0.f,
					  0.f, 0.f, 0.f, 0.f,
					  0.f, 0.f, 0.f, 0.f,
					  0.f, 0.f, 0.f, 0.f);

	float invdet = 1.f / det;

	result._11 = (m_matrix.m[1][1] * c5 - m_matrix.m[1][2] * c4 + m_matrix.m[1][3] * c3) * invdet;
    result._12 = (-m_matrix.m[0][1] * c5 + m_matrix.m[0][2] * c4 - m_matrix.m[0][3] * c3) * invdet;
    result._13 = (m_matrix.m[3][1] * s5 - m_matrix.m[3][2] * s4 + m_matrix.m[3][3] * s3) * invdet;
    result._14 = (-m_matrix.m[2][1] * s5 + m_matrix.m[2][2] * s4 - m_matrix.m[2][3] * s3) * invdet;

    result._21 = (-m_matrix.m[1][0] * c5 + m_matrix.m[1][2] * c2 - m_matrix.m[1][3] * c1) * invdet;
    result._22 = (m_matrix.m[0][0] * c5 - m_matrix.m[0][2] * c2 + m_matrix.m[0][3] * c1) * invdet;
    result._23 = (-m_matrix.m[3][0] * s5 + m_matrix.m[3][2] * s2 - m_matrix.m[3][3] * s1) * invdet;
    result._24 = (m_matrix.m[2][0] * s5 - m_matrix.m[2][2] * s2 + m_matrix.m[2][3] * s1) * invdet;

    result._31 = (m_matrix.m[1][0] * c4 - m_matrix.m[1][1] * c2 + m_matrix.m[1][3] * c0) * invdet;
    result._32 = (-m_matrix.m[0][0] * c4 + m_matrix.m[0][1] * c2 - m_matrix.m[0][3] * c0) * invdet;
    result._33 = (m_matrix.m[3][0] * s4 - m_matrix.m[3][1] * s2 + m_matrix.m[3][3] * s0) * invdet;
    result._34 = (-m_matrix.m[2][0] * s4 + m_matrix.m[2][1] * s2 - m_matrix.m[2][3] * s0) * invdet;
	
	result._41 = (-m_matrix.m[1][0] * c3 + m_matrix.m[1][1] * c1 - m_matrix.m[1][2] * c0) * invdet;
    result._42 = (m_matrix.m[0][0] * c3 - m_matrix.m[0][1] * c1 + m_matrix.m[0][2] * c0) * invdet;
    result._43 = (-m_matrix.m[3][0] * s3 + m_matrix.m[3][1] * s1 - m_matrix.m[3][2] * s0) * invdet;
    result._44 = (m_matrix.m[2][0] * s3 - m_matrix.m[2][1] * s1 + m_matrix.m[2][2] * s0) * invdet;

	return Matrix(result);
}

/**
 * Get the transpose of this matrix
 */
Matrix Matrix::Transpose()
{
	float m12, m13, m14,
		  m23, m24, m34;

	Matrix result = Matrix(m_matrix);

	m12 = result.m_matrix._12;
	m13 = result.m_matrix._13;
	m14 = result.m_matrix._14;
	m23 = result.m_matrix._23;
	m24 = result.m_matrix._24;
	m34 = result.m_matrix._34;

	result.m_matrix._12 = result.m_matrix._21;
	result.m_matrix._13 = result.m_matrix._31;
	result.m_matrix._14 = result.m_matrix._41;
	result.m_matrix._23 = result.m_matrix._32;
	result.m_matrix._24 = result.m_matrix._42;
	result.m_matrix._34 = result.m_matrix._43;

	result.m_matrix._21 = m12;
	result.m_matrix._31 = m13;
	result.m_matrix._41 = m14;
	result.m_matrix._32 = m23;
	result.m_matrix._42 = m24;
	result.m_matrix._43 = m34;

	return result;
}

/**
 * Get as the actual matrix we need for calculations later on.
 */
XMMATRIX Matrix::GetAsXMMATRIX()
{
	XMMATRIX result;
	result = XMLoadFloat4x4(&m_matrix);
	return result;
}

/**
 * = operator for the Matrix
 *
 * @param	Matrix to copy
 */
Matrix& Matrix::operator=(const Matrix &param)
{
	if (this == &param)
      return *this;

	m_matrix = param.m_matrix;

	return *this;
}

/**
 * + operator for the Matrix
 *
 * @param	Matrix to add
 */
const Matrix Matrix::operator+ (const Matrix &param) const
{
	Matrix result = *this;

	for (unsigned int i = 0; i < 4; i++)
		for (unsigned int j = 0; j < 4; j++)
			result.m_matrix.m[i][j] += param.m_matrix.m[i][j];

	return result;
}

/**
 * - operator for the Matrix
 *
 * @param	Matrix to subtract
 */
const Matrix Matrix::operator- (const Matrix &param) const
{
	Matrix result = *this;

	for (unsigned int i = 0; i < 4; i++)
		for (unsigned int j = 0; j < 4; j++)
			result.m_matrix.m[i][j] -= param.m_matrix.m[i][j];

	return result;
}

/**
 * * operator for the Matrix
 *
 * @param	Matrix to multiply
 */
const Matrix Matrix::operator* (const Matrix &param) const
{
	Matrix initial = *this;
	XMFLOAT4X4 result;

	for (unsigned int i = 0; i < 4; i++)
		for (unsigned int j = 0; j < 4; j++)
			result.m[i][j]  = (initial.m_matrix.m[i][0] * param.m_matrix.m[0][j]) + 
							  (initial.m_matrix.m[i][1] * param.m_matrix.m[1][j]) + 
							  (initial.m_matrix.m[i][2] * param.m_matrix.m[2][j]) + 
							  (initial.m_matrix.m[i][3] * param.m_matrix.m[3][j]);

	return Matrix(result);
}

/**
 * * operator for the Matrix
 *
 * @param	Scalar to multiply
 */
const Matrix Matrix::operator* (const float &param) const
{
	Matrix result = *this;

	for (unsigned int i = 0; i < 4; i++)
		for (unsigned int j = 0; j < 4; j++)
			result.m_matrix.m[i][j] *= param;

	return result;
}

/**
 * * operator for the Matrix
 *
 * @param	Scalar to multiply
 */
const Matrix Matrix::operator/ (const float &param) const
{
	Matrix result = *this;

	for (unsigned int i = 0; i < 4; i++)
		for (unsigned int j = 0; j < 4; j++)
			result.m_matrix.m[i][j] /= param;

	return result;
}

/**
 * () operator for the Matrix, to access singular elements - getter
 *
 * @column	Column to get
 * @row		Row to get
 */
const float Matrix::operator() (const unsigned int &column, const unsigned int &row) const
{
	return m_matrix.m[column][row];
}

/**
 * () operator for the Matrix, to access singular elements - setter
 *
 * @column	Column to get
 * @row		Row to get
 */
float& Matrix::operator() (const unsigned int &column, const unsigned int &row)
{
	return m_matrix.m[column][row];
}

/**
 * * operator for the Matrix, in the other direction
 *
 * @param	Scalar to multiply
 */
inline Matrix operator* (float s, const Matrix& v)
{
	return v * s;
}