////////////////////////////////////////////////////////////////////////////////
// Filename: CGFInclude.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Defined to allow for exporting class/methods as a class library
#ifdef CGAMEFRAMEWORK_EXPORTS
    #define CGF_API __declspec(dllexport)
#else
    #define CGF_API __declspec(dllimport)
#endif

//Disable the dll-interface warnign for XMMATRIX as we are only exposing this through methods, not the matrix struct itself.
// Thus, we can ignore the warning: http://www.cplusplus.com/forum/beginner/13928/
#pragma warning (disable : 4251)