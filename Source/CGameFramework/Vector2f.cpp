////////////////////////////////////////////////////////////////////////////////
// Filename: Vector2f.cpp
////////////////////////////////////////////////////////////////////////////////

#include "Vector2f.h"

using namespace CGF;

/**
 * Constructor for the Vector2f
 */
Vector2f::Vector2f()
{
	this->X = 0.0f;
	this->Y = 0.0f;
}

/**
 * Copy Constructor for the Vector2f
 *
 * copy		Vector to copy
 */
Vector2f::Vector2f(const Vector2f &copy)
{
	X = copy.X;
	Y = copy.Y;
}

/**
 * Constructor for the Vector2f
 *
 * @x	Position in the X axis
 * @y	Position in the Y axis
 */
Vector2f::Vector2f(float x, float y)
{
	this->X = x;
	this->Y = y;
}

/**
 * Destructor for the Vector2f
 */
Vector2f::~Vector2f()
{
}

/**
 * Calculate the length for the vectior
 */
float Vector2f::Length()
{
	return sqrt((X * X) + (Y * Y));
}

/**
 * Normalize this vector
 */
void Vector2f::Normalize()
{
	float length = Length();

	if (length != 0)
	{
		X /= length;
		Y /= length;
	}
}
/**
 * = operator for the Vector2f
 *
 * @param	Vector2f to copy
 */

Vector2f& Vector2f::operator=(const Vector2f &param)
{
	if (this == &param)
      return *this;

	X = param.X;
	Y = param.Y;

	return *this;
}

/**
 * += operator for the Vector2f
 *
 * @param	Vector2f to add
 */
Vector2f& Vector2f::operator+= (const Vector2f &param)
{
	X += param.X;
	Y += param.Y;

	return *this;
}

/**
 * -= operator for the Vector2f
 *
 * @param	Vector2f to subtract
 */
Vector2f& Vector2f::operator-= (const Vector2f &param)
{
	X -= param.X;
	Y -= param.Y;

	return *this;
}

/**
 * *= operator for the Vector2f
 *
 * @param	Scalar to multiply
 */
Vector2f& Vector2f::operator*= (const float &param)
{
	X *= param;
	Y *= param;

	return *this;
}

/**
 * /= operator for the Vector2f
 *
 * @param	Scalar to divide
 */
Vector2f& Vector2f::operator/= (const float &param)
{
	X /= param;
	Y /= param;

	return *this;
}

/**
 * + operator for the Vector2f
 *
 * @param	Vector2f to add
 */
const Vector2f Vector2f::operator+ (const Vector2f &param) const
{
	Vector2f result = *this; 
	result += param;
	return result;
}

/**
 * - operator for the Vector2f
 *
 * @param	Vector2f to subtract
 */
const Vector2f Vector2f::operator- (const Vector2f &param) const
{
	Vector2f result = *this;
	result -= param;
	return result;
}

/**
 * * operator for the Vector2f
 *
 * @param	Scalar to multiply
 */
const Vector2f Vector2f::operator* (const float &param) const
{
	Vector2f result = *this;
	result *= param;
	return result;
}

/**
 * / operator for the Vector2f
 *
 * @param	Scalar to divide
 */
const Vector2f Vector2f::operator/ (const float &param) const
{
	Vector2f result = *this;
	result /= param;
	return result;
}