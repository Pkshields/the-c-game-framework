////////////////////////////////////////////////////////////////////////////////
// Filename: CGFMath.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include "CGFInclude.h"
#include "Vector2f.h"
#include "Vector3f.h"
#include "Matrix.h"
#include <math.h>

using namespace CGF;

//Defines
#define DEGREE_IN_RADIANS 0.0174532925f
#define PI 3.14159265359

//Methods
CGF_API float DegreesToRadians(float degrees);
CGF_API float RadiansToDegrees(float degrees);

Matrix CreateTranslateMatrix(const Vector2f translation);
Matrix CreateTranslateMatrix(const Vector3f translation);

Matrix CreateRotationXMatrix(const float angle);
Matrix CreateRotationYMatrix(const float angle);
Matrix CreateRotationZMatrix(const float angle);

Matrix Create2DScaleMatrix(const float scaleFactor);
Matrix Create3DScaleMatrix(const float scaleFactor);