////////////////////////////////////////////////////////////////////////////////
// Filename: CGFMath.h
////////////////////////////////////////////////////////////////////////////////

#include "CGFMath.h"

/**
 * Convert an angle from degrees to radians
 *
 * @degrees	Angle in Degrees
 */
float DegreesToRadians(float degrees)
{
	return degrees * 0.0174532925f;
}

/**
 * Convert an angle from radians to degrees
 *
 * @radians	Angle in Radians
 */
float RadiansToDegrees(float radians)
{
	return radians * 57.2957795786f;
}

/**
 * Calculate a translation matrix
 *
 * @translation		Translation in 2D form
 */
Matrix CreateTranslateMatrix(const Vector2f translation)
{
	Matrix matrix = Matrix();

	matrix(3, 0) = translation.X;
    matrix(3, 1) = translation.Y;
    matrix(3, 2) = 0.f;

    return matrix;
}

/**
 * Calculate a translation matrix
 *
 * @translation		Translation in 3D form
 */
Matrix CreateTranslateMatrix(const Vector3f translation)
{
	Matrix matrix = Matrix();

	matrix(3, 0) = translation.X;
    matrix(3, 1) = translation.Y;
    matrix(3, 2) = translation.Z;

    return matrix;
}

/**
 * Calculate a rotation matrix around the X axis
 *
 * @angle	Angle to rotate in degrees
 */
Matrix CreateRotationXMatrix(const float angle)
{
	//Calculate the angle in radians
	float radians = DegreesToRadians(fmodf(angle, 360.f));
	float cosVal = cos(radians);
	float sinVal = sin(radians);

	//Calculate the rotation in the X axis from this
	Matrix matrix = Matrix();

	matrix(1, 1) = cosVal;
    matrix(1, 2) = sinVal;
    matrix(2, 1) = -sinVal;
	matrix(2, 2) = cosVal;

    return matrix;
}

/**
 * Calculate a rotation matrix around the Y axis
 *
 * @angle	Angle to rotate in degrees
 */
Matrix CreateRotationYMatrix(const float angle)
{
	//Calculate the angle in radians
	float radians = DegreesToRadians(fmodf(angle, 360.f));
	float cosVal = cos(radians);
	float sinVal = sin(radians);

	//Calculate the rotation in the Y axis from this
	Matrix matrix = Matrix();

	matrix(0, 0) = cosVal;
    matrix(0, 2) = -sinVal;
    matrix(2, 0) = sinVal;
	matrix(2, 2) = cosVal;

    return matrix;
}

/**
 * Calculate a rotation matrix around the Z axis
 *
 * @angle	Angle to rotate in degrees
 */
Matrix CreateRotationZMatrix(const float angle)
{
	//Calculate the angle in radians
	float radians = DegreesToRadians(fmodf(angle, 360.f));
	float cosVal = cos(radians);
	float sinVal = sin(radians);

	//Calculate the rotation in the Z axis from this
	Matrix matrix = Matrix();

	matrix(0, 0) = cosVal;
    matrix(0, 1) = sinVal;
    matrix(1, 0) = -sinVal;
	matrix(1, 1) = cosVal;

    return matrix;
}

/**
 * Calculate a scale matrix for a 2D object
 *
 * @scaleFactor		Amount to scale by
 */
Matrix Create2DScaleMatrix(const float scaleFactor)
{
	Matrix matrix = Matrix();

	matrix(0, 0) = scaleFactor;
    matrix(1, 1) = scaleFactor;

    return matrix;
}

/**
 * Calculate a scale matrix for a 3D object
 *
 * @scaleFactor		Amount to scale by
 */
Matrix Create3DScaleMatrix(const float scaleFactor)
{
	Matrix matrix = Matrix();

	matrix(0, 0) = scaleFactor;
    matrix(1, 1) = scaleFactor;
    matrix(2, 2) = scaleFactor;

    return matrix;
}