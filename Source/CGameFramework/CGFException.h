////////////////////////////////////////////////////////////////////////////////
// Filename: CGFException.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include <stdexcept>

namespace CGF
{
	class CGFException : public std::runtime_error
	{
	public:
		CGFException(const char* exception) : std::runtime_error(exception) { }
	};
}