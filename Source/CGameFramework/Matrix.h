////////////////////////////////////////////////////////////////////////////////
// Filename: Vector3f.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include "CGFInclude.h"
#include <math.h>
#include <DirectXMath.h>

using namespace DirectX;

namespace CGF
{
	class CGF_API Matrix
	{
	public:
		Matrix();
		Matrix(const Matrix &copy);
		Matrix(float m11, float m12, float m13, float m14,
			   float m21, float m22, float m23, float m24, 
			   float m31, float m32, float m33, float m34, 
			   float m41, float m42, float m43, float m44);
		Matrix(XMFLOAT4X4& matrix);
		~Matrix();

		static Matrix Identity();

		Matrix Inverse();
		Matrix Transpose();

		XMMATRIX GetAsXMMATRIX();

		Matrix& operator=(const Matrix &param);
		const Matrix operator+ (const Matrix &param) const ;
		const Matrix operator- (const Matrix &param)const ;
		const Matrix operator* (const Matrix &param)const ;
		const Matrix operator* (const float &param)const ;
		const Matrix operator/ (const float &param)const ;
		const float operator() (const unsigned int &column, const unsigned int &row)const ;
		float& operator() (const unsigned int &column, const unsigned int &row);

	private:
		XMFLOAT4X4 m_matrix;
	};
}