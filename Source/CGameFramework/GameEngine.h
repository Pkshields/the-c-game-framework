////////////////////////////////////////////////////////////////////////////////
// Filename: GameEngine.h
////////////////////////////////////////////////////////////////////////////////

//Preprocessor directive designed to cause the current source file to be included only once in a single compilation
#pragma once

//Includes
#include "CGFInclude.h"
//#include <windows.h>
#include "Window.h"

namespace CGF
{
	class CGF_API GameEngine
	{
	public:
		GameEngine();
		~GameEngine();

		bool Initialize(const int windowWidth, const int windowHeight, const bool isWindowFullscreen);
		void Shutdown();
		void Run();

	private:
		bool Frame();

	private:
		Window* m_window;				/**< Window for this current system */
	};
}