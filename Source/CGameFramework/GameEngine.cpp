////////////////////////////////////////////////////////////////////////////////
// Filename: GameEngine.cpp
////////////////////////////////////////////////////////////////////////////////

#include "GameEngine.h"

using namespace CGF;

/**
 * Create an instance of the GameEngine
 */
GameEngine::GameEngine()
{
	m_window = 0;
}

/**
 * Deconstructor of the GameEngine
 */
GameEngine::~GameEngine()
{
}

/**
 * Initialize the GameEngine
 *
 * @param windowWidth			Width of the window
 * @param windowHeight			Height of the window
 * @param isWindowFullscreen	Is the window fullscreen? (ignores the above values if it is)
 * @return Result of initialization
 */
bool GameEngine::Initialize(const int windowWidth, const int windowHeight, const bool isWindowFullscreen)
{
	//Variables!
	bool result;

	//Create the window object
	m_window = new Window();
	if(!m_window)
		return false;

	//Initialize the Window
	result = m_window->Initialize(windowWidth, windowHeight, isWindowFullscreen);
	if(!result)
		return false;
	
	return true;
}

/**
 * Shut down the GameEngine
 */
void GameEngine::Shutdown()
{
	//Release the Window object.
	if(m_window)
	{
		m_window->Shutdown();
		delete m_window;
		m_window = 0;
	}
}

/**
 * Run the game
 */
void GameEngine::Run()
{
	//Variables!
	MSG msg;			//Window messages container
	bool done, result;	//Hella Bools

	//Initialize the message structure.
	ZeroMemory(&msg, sizeof(MSG));
	
	//Loop until there is a quit message from the window or the user.
	done = false;
	while(!done)
	{
		//Handle the windows messages.
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		//If windows signals to end the application then exit out.
		if(msg.message == WM_QUIT)
			done = true;
		
		else
		{

		}
	}
}