# The C++ Game Framework
 
The C++ Game Framework is a DirectX 11 game framework, built in C++ and DirectX 11. This is used to contain many to the DirectX functionality and wrap them into a framework to make game development easier. 

The C++ Game Framework is licensed under the New BSD License.